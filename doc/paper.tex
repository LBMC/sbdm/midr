\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\argmin}{arg\,min}
\title{Multivariate IDR}

\begin{document}

\section{Introduction}

Evaluating the reproducibility of scientific experiment is essential to reliable scientific progress.
The Irreproducible Discovery Rate (IDR) was introduced by \cite{liMeasuringReproducibilityHighthroughput2011a} to identify which Chip-Seq peak are reproducible between two replicate of the ENCODE project.

\section{Methods}

\paragraph{Sklar's Theorem \cite{sklarFonctionsRepartitionDimensions1959}}: Let $H$ be a joint distribution function with margin $F_1, \dots, F_n$. Then there exists a copula $C$ such that

\begin{equation} \label{sklartheo}
H\left(x_1, \dots, x_m\right) = C\left(F_1\left(x_1\right), \dots, F_m\left(x_m\right)\right)
\end{equation}

If $F_1, \dots, F_m$ are continuous then $C$ is unique. Conversely, if $C$ is a copula and $F_1, \dots, F_m$ are distribution functions, then the function $H$ is a joint distribution function with margins $F_1, \dots, F_m$.

\paragraph{Property 1} Let $G\left(X_1, \dots, X_m\right)$ be a joint distribution of $m$ random variables $X_1, \dots, X_m$, $F_1\left(x_1\right), \dots, F_m\left(x_m\right)$ are marginal distribution functions of these random variables and $C\left(u_1, \dots, u_m\right)$ is the corresponding copula, then for every $\boldsymbol{u} = \left(u_1, \dots, u_m\right) \in \left[0, 1\right]^n$ it satisfies that

\begin{equation} \label{sklarprop1}
C\left(u_1, \dots, u_m\right) = \
G\left(F_{1}^{-1}\left(x_1\right), \dots, F_{m}^{-1}\left(x_m\right)\right)
\end{equation}

where $F_{i}^{-1}\left(u_i\right) = \inf\left\{x:F_{j}\left(x\right) \geq u_i\right\}$ is the right-continuous inverse of $F_i$, defined as $F_{i}^{-1}\left(u_i\right)$

\paragraph{Property 2} Let Let $G\left(X_1, \dots, X_m\right)$ be a joint distribution of $m$ random variables $X_1, \dots, X_m$, $C\left(u_1, \dots, u_m\right)$ be a copula, $c\left(u_1, \dots, u_m\right)$ be its density function and $F_1\left(x_1\right), \dots, F_m\left(x_m\right)$ are marginal distribution functions of the random variables, we can get that

\begin{equation} \label{sklarprop2}
g\left(X_1, \dots, X_m\right) = \
c\left(F_1\left(X_1\right), \dots, F_m\left(X_m\right)\right) \prod_{i=1}^{m}f_{i}\left(X_i\right)
\end{equation}

where $c\left(u_1, \dots, u_m\right) = \frac{\partial C\left(u_1, \dots, u_m\right)}{\partial u_1 \dots \partial u_m}$ and $f_{i}\left(X_i\right)$ and $g\left(X_1, \dots, X_m\right)$ are density function of $F_{i}\left(X_i\right)$ and $G\left(X_1, \dots, X_m\right)$, respectively.

Currently there are two main types of copulas, the Elliptical copulas and the Archimedean copulas.
Elliptical copulas have contoured distribtions, such as Gaussian copula. Archimedean copula are an associative class of copulas satifying the following equations:

\begin{equation} \label{archimedean}
C\left(u_1, \dots, u_m\right) = \
\psi^{-1}\left(\psi\left(u_1\right) + \dots + \psi\left(u_m\right)\right)
\end{equation}

where $\psi\left(\bullet\right)$ is called the generator of the Archimedean copula.

\paragraph{Theorem 2 \cite{zhangMeasuringReproducibilityHighThroughput2013}} A function is called mixture copula if it satisfies: $C_{T}\left(u_1, \dots, u_m\right) = \sum_{\tau=1}^{T} \alpha_{\tau}C_{\tau}\left(u_1, \dots, u_m\right)$ with $0 \leq \alpha_i \leq 1$ and $\sum_{\tau=1}^{T} \alpha_1 = 1$. Here $C_1\left(u_1, \dots, u_m\right), \dots, C_{\tau}\left(u_1, \dots, u_m\right)$ are copulas and $\alpha_1, \dots, \alpha_t$ are linear coefficients of $C_{T}\left(u_1, \dots, u_m\right)$. Then $C_{T}\left(u_1, \dots, u_m\right)$ is a copula.

We assume $n$ putative signals measured on $m$ samples with the corresponding vectore of scores for signal $i$ begin $\left(x_{i,1}, \dots, x_{i,m}\right)$. Here $x_{i,j}$ is ascalar score for the signal $i$ in sample $j$. We assume that the observation consist of a more reproducible group and a less reproducible group with $\pi$ the proportion of less reproducible signal. Let $K_i$ be an indicator to identify whether a signal belong the the less reproducible group ($K-i = 0$) or to the more reproducible group ($K_i = 1$).

We assume that the observations of signals come from a more and a less reproducible groups. According to \cite{sklartheo}, any multivariate distribtion can be divided into its marginal probability distribtions and its copula.
Thus we can construct the foolowing parametric model:

Let $K_i \sim \text{Bernouilli}\left(\pi\right)$ and $\left(x_{i,1}, \dots, x_{i,m}\right) | K_i \sim S_{k}\left(u_1, \dots, \u_m\right), k = 0,1$ and

\begin{equation} \label{Skfunction}
S_{k}\left(x_{i,1}, \dots, x_{i,m}\right) = \
C\left(F_{1}\left(x_{i,1}\right), \dots, F_{m}\left(x_{i,m}\right); \theta_k\right), k=0,1
\end{equation}

where $F_{j}\left(x_{i,j}\right)$ are the marginal distribtions of $x_{i,j}$, and $\theta_k$ the parameter of the copula $C$.

To model the irreproducible component ($K_i = 0$), we use the product copula (or independence copula):

\begin{equation}
S_{0}\left(x_{i,1}, \dots, x_{i,m}\right) = \prod_{j=1}^{m}F_{j}\left(x_{i,j}\right)
\end{equation}

To model the reproducible component ($K_i = 0$), we use a mixture of Archimedean copula:
Let $L_i \sim \text{Multinomial}\left(\boldsymbol\alpha\right)$, $\boldsymbol\alpha = \left(\alpha_1, \dots, \alpha_{T}\right)$, and $\boldsymbol\theta = \left(\theta_1, \dots, \theta_{T}\right)$ the corresponding copula parameters, we have $\left(x_{i,1}, \dots, x_{i,m}\right) | K_i = 1, L_i \sim C_{\tau}\left(F_{1}\left(x_{i,1}\right), \dots, F_{m}\left(x_{i,m}\right); \theta_{\tau}\right), \tau=1,\dots, T$, thus


\begin{equation}
\begin{split}
S_{1}\left(x_{i,1}, \dots, x_{i,m}\right) & = \sum_{\tau=1}^{T}\alpha_{\tau}C_{\tau}\left(F_{1}\left(x_{i,1}\right), \dots, F_{m}\left(x_{i,m}\right); \theta_{\tau}\right) \\
& = \sum_{\tau=1}^{T}\alpha_{\tau}\psi_{\tau}^{-1}\left(\sum_{j=1}^{n}\psi_{\tau}\left(F_{j}\left(x_{i,j}\right); \theta_{\tau}\right); \theta_{\tau}\right)
\end{split}
\end{equation}

with $0 \leq \alpha_t \leq 1$ and $\sum_{\tau=1}^{T} \alpha_t = 1$

The total distribution function is

\begin{equation} \label{Sfunction}
S\left(x_{i,1}, \dots, x_{i,m}\right) = \
\pi S_{0}\left(x_{i,1}, \dots, x_{i,m}\right) + \
\left(1 - \pi\right) S_{1}\left(x_{i,1}, \dots, x_{i,m}\right)
\end{equation}

From \cite{sklarprop2} we can get the density function of $S\left(x_{i,1}, \dots, x_{i,m}\right)$ as follow:

\begin{equation} \label{sfunction}
s\left(x_{i,1}, \dots, x_{i,m}\right) = \
\pi s_{0}\left(x_{i,1}, \dots, x_{i,m}\right)\prod_{j=1}^{m}f_{j}\left(x_{i,j}\right) + \
\left(1 - \pi\right) s_{1}\left(x_{i,1}, \dots, x_{i,m}\right)\prod_{j=1}^{m}f_{j}\left(x_{i,j}\right)
\end{equation}

Thus the likelihood of the model parametrized by $\boldsymbol\Lambda = \left(\pi, \boldsymbol\alpha, \boldsymbol\theta\right)$ and $F_1, \dots, F_m$ is:

\begin{equation} \label{likelihood}
\begin{split}
L\left(\boldsymbol\Lambda\right) &= \prod_{i=1}^{n}s\left(x_{i,1}, \dots, x_{i,m}\right) \\
& = \prod_{i=1}^{n} \left[\pi \prod_{j=1}^{m}f_{j}\left(x_{i,j}\right) + \left(1 - \pi\right) \sum_{\tau=1}^{T} \left(\alpha_{\tau}c_{\tau}\left(F_{1}\left(x_{i,1}\right), \dots, F_{m}\left(x_{i,m}\right); \theta_{\tau}\right)\prod_{j=1}^{m}f_{j}\left(x_{i,j}\right)\right)\right] \\
& = \prod_{i=1}^{n} \prod_{j=1}^{m}f_{j}\left(x_{i,j}\right) \left[\pi + \left(1 - \pi\right) \sum_{\tau=1}^{T} \left(\alpha_{\tau}c_{\tau}\left(F_{1}\left(x_{i,1}\right), \dots, F_{m}\left(x_{i,m}\right); \theta_{\tau}\right)\right)\right] \\
\log L\left(\boldsymbol\Lambda\right) &= \sum_{i=1}^{n} \left[\sum_{j=1}^{m}\log{f_{j}\left(x_{i,j}\right)} + \log\left(\pi + \left(1 - \pi\right) \sum_{\tau=1}^{T} \left(\alpha_{\tau}c_{\tau}\left(F_{1}\left(x_{i,1}\right), \dots, F_{m}\left(x_{i,m}\right); \theta_{\tau}\right)\right)\right)\right]
\end{split}
\end{equation}

Here we describe the EM algorithm. To proceed,we denote $K_i$ and $L_{i,t}$ as the latent variables, then the complete log-likelihood is:

\begin{align*} \label{loglikelihood}
\log L\left(\boldsymbol\Lambda, \boldsymbol{K}, \boldsymbol{L}\right) & = \sum_{i=1}^{n} \Bigg[\sum_{j=1}^{m} \log{f_{j}\left(x_{i,j}\right)} + \\
& \log\left(\left(1 - K_{i}\right)\pi + K_{i}\left(1 - \pi\right) \times \sum_{\tau=1}^{T} L_{i, t}\alpha_{\tau}c_{\tau}\left(F_{1}\left(x_{i,1}\right), \dots, F_{m}\left(x_{i,m}\right); \theta_{\tau}\right)\right)
& \Bigg]
\end{align*}

E-step:

\begin{align*}
Q\left(\boldsymbol\Lambda, \boldsymbol\Lambda^{\left(t\right)}\right) & = \sum_{i=1}^{n} \Bigg[\sum_{j=1}^{m} \log{f_{j}\left(x_{i,j}\right)} + \log\Bigg( \\
& \Pr\left(K_{i} = 0 \right)\pi^{\left(t\right)} + \\
& \Pr\left(K_{i} = 1\right)\left(1 - \pi^{\left(t\right)}\right) \sum_{\tau=1}^{T} \Pr\left(L_{i, t} = 1\right)\alpha_{\tau}^{\left(t\right)}c_{\tau}\left(F_{1}\left(x_{i,1}\right), \dots, F_{m}\left(x_{i,m}\right); \theta_{\tau}^{\left(t\right)}\right) \Bigg)\Bigg]
\end{align*}

Then:

\begin{align*}
K_{i}^{\left(t+1\right)} & = E\left(K_{i} | \boldsymbol{x}_{i}, \boldsymbol\Lambda^{\left(t\right)}\right) \\
& = \frac{\Pr\left(K_{i} = 1, \boldsymbol{x}_{i} | \boldsymbol\Lambda^{\left(t\right)}\right)}{\Pr\left(\boldsymbol{x}_{i} | \boldsymbol\Lambda^{\left(t\right)}\right)} \\
& = \frac{1 - \Pr\left(K_{i} = 0, \boldsymbol{x}_{i} | \boldsymbol\Lambda^{\left(t\right)}\right)}{\Pr\left(\boldsymbol{x}_{i} | \boldsymbol\Lambda^{\left(t\right)}\right)} \\
& = \frac{\left(1 - \pi^{\left(t\right)}\right)\sum_{\tau=1}^{T} \alpha_{\tau}^{\left(t\right)}c_{\tau}\left(F_{1}\left(x_{i,1}\right), \dots, F_{m}\left(x_{i,m}\right); \theta_{\tau}^{\left(t\right)}\right)}{\pi^{\left(t\right)} + \left(1 - \pi^{\left(t\right)}\right) \sum_{\tau=1}^{T} \alpha_{\tau}^{\left(t\right)}c_{\tau}\left(F_{1}\left(x_{i,1}\right), \dots, F_{m}\left(x_{i,m}\right); \theta_{\tau}^{\left(t\right)}\right)}
\end{align*}

\begin{align*}
L_{i, \tau}^{\left(t+1\right)} & = E\left(L_{i, \tau} | \boldsymbol{x}_{i}, \boldsymbol\Lambda^{\left(t\right)}\right) \\
& = \frac{\Pr\left(L_{i, \tau} = 1, \boldsymbol{x}_{i} | \boldsymbol\Lambda^{\left(t\right)}\right)}{\Pr\left(\boldsymbol{x}_{i} | \boldsymbol\Lambda^{\left(t\right)}\right)} \\
& = \frac{\alpha_{\tau}^{\left(t\right)}c_{\tau}\left(F_{1}\left(x_{i,1}\right), \dots, F_{m}\left(x_{i,m}\right); \theta_{\tau}^{\left(t\right)}\right)}{\sum_{\ell=1}^{T} \alpha_{\ell}^{\left(t\right)}c_{\ell}\left(F_{1}\left(x_{i,1}\right), \dots, F_{m}\left(x_{i,m}\right); \theta_{\ell}^{\left(t\right)}\right)}
\end{align*}

M-Step:

The MLE of the mixing proportions are:

\begin{equation}
\pi^{\left(t+1\right)} = \frac{\sum_{i=1}^{n} K_{i}^{\left(t+1\right)}}{n}
\end{equation}

\begin{equation}
\alpha_{\tau}^{\left(t+1\right)} = \frac{\sum_{i=1}^{n} L_{i,\tau}^{\left(t+1\right)}}{n}
\end{equation}

with $\sum_{\tau=1}^{T}\alpha_{\tau}^{\left(t+1\right)} = 1$

The MLE estimator of the $\theta$'s are difficult to compute, sure we use numerical minimization to find:

\begin{equation}
\theta_{\tau}^{\left(t+1\right)} = \argmin_{\theta_{\tau}} -\sum_{i=1}^{n} \log\left(\pi + \left(1 - \pi\right) \sum_{\ell=1}^{T} \left(\alpha_{\ell}c_{\ell}\left(F_{1}\left(x_{i,1}\right), \dots, F_{m}\left(x_{i,m}\right); \theta_{\ell}\right)\right)\right)
\end{equation}

with $\ell \in \left[1, \dots, T\right]$

Finally, we can estimate the irreproducibility of each signal based on:

\begin{equation}
\Pr\left(K_{i} = 0 | \boldsymbol\Lambda\right) = \frac{\pi}{\pi + \left(1 - \pi\right) \sum_{\tau=1}^{T} \left(\alpha_{\tau}c_{\tau}\left(F_{1}\left(x_{i,1}\right), \dots, F_{m}\left(x_{i,m}\right); \theta_{\tau}\right)\right)}
\end{equation}

\section{Multivariate IDR with sensured data}

In case of missing data, some values of $\boldsymbol{x_i} = \left(x_{1}, \dots, x_{m}\right)$ are missing. Let $\boldsymbol{z_{i}} \in {0,1}^{m}$ an observed indicator variable with $z_{i,j} = 0$ if $x_{i,j}$ is missing and 1 otherwise. We split the $x_i$'s into $x_i^{obs}$ and $x_i^{abs}$.

Therefore the distribution of the irreproducible component become:

\begin{equation}
S_{0}\left(\boldsymbol{x_{i}}, \boldsymbol{z_{i}}\right) = \prod_{j=1}^{m}z_{i,j}F_{j}\left(x_{i,j}\right) \times \prod_{j=1}^{m} \left(1 - z_{i,j}\right) \int_{x} F_{j}\left(x\right) dx
\end{equation}

And the reproducible component ($K_i = 0$), become:

\begin{equation}
S_{1}\left(\boldsymbol{x_{i}}, \boldsymbol{z_{i}}\right) = \sum_{\tau=1}^{T}\alpha_{\tau}\psi_{\tau}^{-1}\left(\sum_{j=1}^{n} z_{i,j}\psi_{\tau}\left(F_{j}\left(x_{i,j}\right); \theta_{\tau}\right) + \sum_{j=1}^{n} \left(1 - z_{i,j}\right)\int_{x}\psi_{\tau}\left(F_{j}\left(x\right); \theta_{\tau}\right) dx; \theta_{\tau}\right)
\end{equation}

with $0 \leq \alpha_t \leq 1$ and $\sum_{\tau=1}^{T} \alpha_t = 1$

The total distribution function is

\begin{equation} \label{Sfunction}
S\left(x_{i,1}, \dots, x_{i,m}\right) = \
\pi S_{0}\left(x_{i,1}, \dots, x_{i,m}\right) + \
\left(1 - \pi\right) S_{1}\left(x_{i,1}, \dots, x_{i,m}\right)
\end{equation}

The complete likelihood of the model is:

\begin{equation} \label{likelihood}
\begin{split}
L\left(\boldsymbol\Lambda\right) &= \prod_{i=1}^{n}s\left(x_{i,1}, \dots, x_{i,m}\right) \\
& = \prod_{i=1}^{n} \left[\pi \prod_{j=1}^{m}f_{j}\left(x_{i,j}\right) + \left(1 - \pi\right) \sum_{\tau=1}^{T} \left(\alpha_{\tau}c_{\tau}\left(F_{1}\left(x_{i,1}\right), \dots, F_{m}\left(x_{i,m}\right); \theta_{\tau}\right)\prod_{j=1}^{m}f_{j}\left(x_{i,j}\right)\right)\right] \\
& = \prod_{i=1}^{n} \prod_{j=1}^{m}f_{j}\left(x_{i,j}\right) \left[\pi + \left(1 - \pi\right) \sum_{\tau=1}^{T} \left(\alpha_{\tau}c_{\tau}\left(F_{1}\left(x_{i,1}\right), \dots, F_{m}\left(x_{i,m}\right); \theta_{\tau}\right)\right)\right] \\
\log L\left(\boldsymbol\Lambda\right) &= \sum_{i=1}^{n} \left[\sum_{j=1}^{m}\log{f_{j}\left(x_{i,j}\right)} + \log\left(\pi + \left(1 - \pi\right) \sum_{\tau=1}^{T} \left(\alpha_{\tau}c_{\tau}\left(F_{1}\left(x_{i,1}\right), \dots, F_{m}\left(x_{i,m}\right); \theta_{\tau}\right)\right)\right)\right]
\end{split}
\end{equation}



\end{document}
