# small tutorial to reproduce my results with midr

## Run the chip-seq pipeline with a modified macs2 option

We want to run `macs2`, without a filter on the peak quality. By default, `macs2` use an FDR threshold of `0.05` on the peaks.
As we don't trust `macs2` model to perform this kind of test without replicate, we want to use a permissive filter, to be 
able to tell appart reproducible from irreproducible peaks with `midr` afterward.
Therefore we need to run `macs2` with the option `--qvalue 0.99`

In addition we need to perform the peak calling on the merged bam. This is why we create the file `wt_all.fastq` and `set2_all.fastq` with the following command on the PSMN:

```
cd /scratch/Bio/lmodolo/ChIPseq_pipeline/data/fastq
zcat CLBT37.WT_rep1.fastq.gz CLBT38.WT_rep2.fastq.gz CLBT39.WT_rep3.fastq.gz | gzip > wt_all.fastq.gz
zcat CLBT40.set2_rep1.fastq.gz CLBT41.set2_rep2.fastq.gz CLBT42.set2_rep3.fastq.gz | gzip > set2_all.fastq.gz
cd /scratch/Bio/lmodolo/ChIPseq_pipeline
```

and modify the file `data/fromPATH_cec.csv` accordingly:

```
ip      input
data/fastq/CLBT37.WT_rep1.fastq.gz      data/fastq/CLBT43.wt_input.fastq.gz
data/fastq/CLBT38.WT_rep2.fastq.gz      data/fastq/CLBT43.wt_input.fastq.gz
data/fastq/CLBT39.WT_rep3.fastq.gz      data/fastq/CLBT43.wt_input.fastq.gz
data/fastq/wt_all.fastq.gz      data/fastq/CLBT43.wt_input.fastq.gz
data/fastq/CLBT40.set2_rep1.fastq.gz    data/fastq/CLBT44.set2_input.fastq.gz
data/fastq/CLBT41.set2_rep2.fastq.gz    data/fastq/CLBT44.set2_input.fastq.gz
data/fastq/CLBT42.set2_rep3.fastq.gz    data/fastq/CLBT44.set2_input.fastq.gz
data/fastq/set2_all.fastq.gz    data/fastq/CLBT44.set2_input.fastq.gz
```

Then I ran the `src/chip_analysis.sh` script in the `/scratch/Bio/lmodolo/ChIPseq_pipeline`

## computing the IDR 

Computing the IDR, Irreproducible Discovery Rate is a way of modeling the data as belonging to one of two groups. An
irreproducible group where signal is random across replicates and a reproducible group where the signal covary across groups.
The IDR, is the probability that a peak belongs to the first group. The goal is to select peak belonging to the second group.

For simplicity I moved all the `.narrowPeak` file in the folder `results/narrowpeak`

```
mkdir results/narrowPeak
fd -I ".*narrowPeak" results/ -x mv {} results/narrowPeak/{/}
```

```
singularity build /Xnfs/abc/singularity/lbmc-midr:1.5.1.img docker://lbmc/midr:1.5.1 # only run once
mkdir -p results/idr/wt/ mkdir -p results/idr/set2/
singularity exec -B /scratch/:/scratch/ /Xnfs/abc/singularity/lbmc-midr:1.5.1.img midr -m results/narrowPeak/wt_all_peaks.narrowPeak -f results/narrowPeak/CLBT*{37,38,39}*.narrowPeak -o results/idr/wt/
singularity exec -B /scratch/:/scratch/ /Xnfs/abc/singularity/lbmc-midr:1.5.1.img midr -m results/narrowPeak/set2_all_peaks.narrowPeak -f results/narrowPeak/CLBT*{40,41,42}*.narrowPeak -o results/idr/set2/
```

The `idr_*.narrowPeak` files in the `results/idr/wt` and `results/idr/set2` now have 2 additional columns, are filtered, to only retain peaks with a corresponding potition in the `*_all_peaks.narrowPeak` file. Peaks not present in the `*_all_peaks.narrowPeak` file or in the other replicate are droped.

The `idr_.narrowPeak` files in the `results/idr/wt` and `results/idr/set2` now have 2 additional columns:
- an IDR column, the probability that the corresponding peak is irreproducible
- an FDR column, if we select the peak with FDR <= x then the selected peaks are reproducible with a average error of x

Here we want to select peaks reproducible in the two experiments `wt` and `set2`, so we need to concatenate the IDR column of
both. And then recompute the FDR column to account for the right number of peaks.

We concatenate the two `idr_*_all_peaks.narrowPeaks` file and we replace the score column by the IDR column to get a regular `.narrowPeak` file.
We filter by IDR < 0.5

```
cat results/idr/wt/idr_wt_all_peaks.narrowPeak \
    results/idr/set2/idr_set2_all_peaks.narrowPeak \
    | sort -k1,1 -k2,2n \
    | awk -v OFS='\t' '{if($11 < 0.5){print $1, $2, $3, $4, $11, $6, $7, $8, $9, $10}}' \
    > results/idr/idr_all_peaks.narrowPeaks
```
`idr_all_peaks.narrowPeaks` is the bed like file that contains the position of all the reproducible peaks.
