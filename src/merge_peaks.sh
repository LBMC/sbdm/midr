#! /bin/sh
cd data/mold/2020_05_12_clone_data/
rm merge.narrowPeak
singularity exec docker://lbmc/bedtools:2.25.0 sh -c "cat *.narrowPeak | sort -k1,1 -k2,2n | bedtools merge -c 4,5,6,7,8,9,10 -o distinct,sum,distinct,sum,sum,sum,sum -i - > merge.narrowPeak"
singularity exec docker://lbmc/bedtools:2.25.0 sh -c "
ls -l *R1*.narrowPeak | \
sed -e 's|.*\s\(.*\)\(R1\)\(.*\)|\1\2\3 \1R2\3 \1merged\3|g' | \
awk '{ system(\"cat \"\$1\" \"\$2\" | sort -k1,1 -k2,2n | bedtools merge -c 4,5,6,7,8,9,10 -o distinct,sum,distinct,sum,sum,sum,sum -i - > \"\$3) }' "
cd ../../..

